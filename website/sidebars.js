module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Техническая документация',
      items: [
        'tech/stack',
        'tech/infra',
      ],
    },
    {
      type: 'category',
      label: 'Docusaurus Tutorial',
      items: [
        'tutorial/getting-started',
        'tutorial/create-a-page',
        'tutorial/create-a-document',
        'tutorial/create-a-blog-post',
        'tutorial/markdown-features',
        'tutorial/thank-you',
      ],
    },
  ],
};
